import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class mathTest {

    @Test
    public void test1() {
        Calculator c = new Calculator();
        double sum = c.add(15.2, 25.8);
        assertEquals(41.0, sum, 0.00001);
    }

    @Test
    public void test2() {
        Calculator c = new Calculator();
        double sub = c.subtract(55.3, 30.7);
        assertEquals(24.6, sub, 0.00001);
    }

    @Test
    public void test3() {
        Calculator c = new Calculator();
        double mult = c.multiply(7.5, 12.5);
        assertEquals(93.75, mult, 0.00001);
    }

    @Test
    public void test4() {
        Calculator c = new Calculator();
        double div = c.divide(45.9, 3.3);
        assertEquals(13.90909, div, 0.00001);
    }
}